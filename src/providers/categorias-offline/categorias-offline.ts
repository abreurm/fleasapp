import { Injectable } from '@angular/core';

/*
  Generated class for the CategoriasOfflineProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriasOfflineProvider { 
  public todas=[
    { nombre: 'Inmuebles', icono: 'home', pagina: '', abrevi: 'inm' },
    { nombre: 'Celulares - Tablets', icono: 'phone-portrait', pagina: '', abrevi: 'cel' },
    { nombre: 'Electrónicos', icono: 'desktop', pagina: '', abrevi: 'elec'},
    { nombre: 'Consolas y videojuegos', icono: 'game-controller-b', pagina: '', abrevi: 'cons' },
    { nombre: 'Artículos para el hogar', icono: 'cafe', pagina: '', abrevi: 'hog' },
    { nombre: 'Ropa y calzado', icono: 'shirt', pagina: '', abrevi: 'rop'},
    { nombre: 'Hobbies y deportes', icono: 'american-football', pagina: '', abrevi: 'hobb' },
    { nombre: 'Mascotas', icono: 'pet', pagina: '', abrevi: 'masc'  },
    { nombre: 'Vehiculos', icono: 'ios-car-outline', pagina: '', abrevi: 'veh' },
    { nombre: 'Empleos y servicios', icono: 'hammer', pagina: '', abrevi: 'serv' }
  ]
  constructor() {
    
  }

}
