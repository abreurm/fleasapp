import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {
  public url: string;
  public userDetails:any;
  public perfil:any;
  
  constructor(public http: HttpClient) {
    this.url = 'http://localhost:8080/api';
  }
  getUsuarioLogin() {
    return this.http.get(this.url + '/usuario');
  }

  getCorreo(email){
    return this.http.get(this.url+'/usuario/email/'+email)
  }

  login(usuario){
  var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  return this.http.post(this.url + '/login', JSON.stringify(usuario), { headers: headers })
      .subscribe((data:any)=>{
      console.log("Enviando usuario");
    },
    (error:any)=>{
      console.dir(error)
    });
  }

  getUsuarioId(){
    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    this.perfil = this.userDetails.usuario; 
    return this.http.get(this.url + '/usuario/' + this.perfil._id)
  }

  guardarUsuario(usuario) {
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

    this.http.post(this.url + '/usuario', JSON.stringify(usuario), { headers: headers })
      .subscribe((data: any) => {
        console.log("Usuario registrado");
      },
        (error: any) => {
          console.dir(error);
        });
  }
}
