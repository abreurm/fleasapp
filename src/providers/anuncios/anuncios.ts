import { Injectable } from '@angular/core';

/*
  Generated class for the AnunciosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AnunciosProvider {
  public listaAnuncios=[
    {
      titulo: 'Red Dead Redemption', 
      precio: '700.000 Bsf',
      uso: 'Nuevo',
      descripcion:'Juego original en formato digital entrega inmediata pregunte por la disponibilidad',
      url: '../assets/imgs/1.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'GTA V ps3',
      precio: '20.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Juego original  entrega inmediata pregunte por la disponibilidad, hacemos entregas personales',
      url: '../assets/imgs/11.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'FIFA 18 ps3',
      precio: '20.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Totalmete nueva, vendo por urgencia economica',
      url: '../assets/imgs/12.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'GT 6',
      precio: '15.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Gran Turismo 6 totalmente nuevo',
      url: '../assets/imgs/10.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'MAX PAYNE 3',
      precio: '1.500.000 Bsf',
      uso: 'Usado',
      descripcion: 'Juego original envio el mismo dia de la compra',
      url: '../assets/imgs/3.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Farcry 3',
      precio: '2.000.000 Bsf',
      uso: 'Usado',
      descripcion: 'pocos meses de uso, vendo por no usar',
      url: '../assets/imgs/5.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Call of Duty Ghosts', 
      precio: '650.000 Bsf',
      uso: 'Nuevo',
      descripcion:'Juego en formato digital original, entrega inmediata',
      url: '../assets/imgs/9.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Call of Duty ',
      precio: '12.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Call of Duty Advanced Warfare juego totalmente nuevo, pregunte por todos nuestros productos',
      url: '../assets/imgs/6.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'God of War collection',
      precio: '12.000.000 Bsf',
      uso: 'Usado',
      descripcion: 'Totalmete nueva, vendo por urgencia economica',
      url: '../assets/imgs/8.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'GT 6',
      precio: '15.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Gran Turismo 6 totalmente nuevo',
      url: '../assets/imgs/10.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'MAX PAYNE 3',
      precio: '1.500.000 Bsf',
      uso: 'Usado',
      descripcion: 'Juego original envio el mismo dia de la compra',
      url: '../assets/imgs/3.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'FIFA 18 Digital',
      precio: '6.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Formato digital',
      url: '../assets/imgs/12.jpg',
      ciudad: 'Distrito Capital'
    }
  
  ];
  listaFav=[
    {
      titulo: 'Huawei P10 lite',
      precio: '85.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Tres meses de garantia pregunte antes de comprar',
      url: '../assets/imgs/p10.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Motorola moto c',
      precio: '60.500.000 Bsf',
      uso: 'Usado',
      descripcion: 'Vendo por motivo de viaje tlf practicamente nuevo sin ningun tipo de detalles',
      url: '../assets/imgs/motoc.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Sueter Jordan original',
      precio: '15.000.000 Bsf',
      uso: 'Nuevo',
      descripcion: 'Comodo y a la moda',
      url: '../assets/imgs/ropa2.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Laptop Dell ',
      precio: '270.000.000 Bsf',
      uso: 'Usado',
      descripcion: '2 meses de uso, vendo por no usar',
      url: '../assets/imgs/laptopdell.jpg',
      ciudad: 'Distrito Capital'
    },
    {
      titulo: 'Samsung S5  ',
      precio: '60.000.000 Bsf',
      uso: 'Usado',
      descripcion: '3 meses de uso, sin ningun tipo de detalles',
      url: '../assets/imgs/s5.jpg',
      ciudad: 'Distrito Capital'
    }
  ]

  listNotificaciones=[
    {
      titulo: 'Samsung S5  ',
      precio: '60.000.000 Bsf',
      desc: 'Un usuario compró tu artículo',
      url: '../assets/imgs/s5.jpg'
    },
    {
      titulo: 'FIFA 18 Digital',
      precio: '6.000.000 Bsf',
      desc: 'Un usuario comentó tu artículo',
      url: '../assets/imgs/12.jpg'
    }
  ]

  listPrincipal=[
    {
      titulo:'Los mejor celulares',
      sub:'Las mejores marcas',
      url:'../assets/imgs/cel.jpg'
    },
    {
      titulo: 'Lo que esta de moda',
      sub: 'Al mejor precio',
      url: '../assets/imgs/ropa3.jpg'
    },
    {
    titulo:'Para las mas femeninas',
    sub:'',
    url:'../assets/imgs/ropa4.jpg'
  },
  {
    titulo: 'Los accesorios que buscas',
    sub: 'En un solo lugar',
    url: '../assets/imgs/zapatos3.jpg'
  },
  {
    titulo: 'Los abrigos que buscas',
    sub: '',
    url: '../assets/imgs/ropa5.jpg'
  }
  ]
}
