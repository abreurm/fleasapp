import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the EstadosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EstadosProvider {
  public url: string;

  constructor(public http: HttpClient) {

    this.url = 'http://localhost:8080/api';
  }

  getEstados(){
    return this.http.get(this.url + '/estados');

  }

  getEstadosID(id) {
    return this.http.get(this.url + '/estados/'+id);

  }
  guardarEstados(estados) {
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

    this.http.post(this.url + '/estados', JSON.stringify(estados), { headers: headers })
      .subscribe((data: any) => {
        console.log("Estado registrado");
      },
        (error: any) => {
          console.dir(error);
        });
  }

}
