import { Injectable } from '@angular/core';

/*
  Generated class for the CiudadProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CiudadProvider {
  public ciudades:Array<{nombre: String,abre:String}>;

  constructor() {
    this.ciudades=[
      {nombre:"Anzoategui",abre:"anz"},
      {nombre:"Aragua",abre:"arg"},
      { nombre: "Barinas", abre: "bar" },
      { nombre: "Carabobo", abre: "carb" },
      { nombre: "Distrito Capital", abre: "dis" },
      { nombre: "Falcon", abre: "fal" },
      { nombre: "Lara", abre: "lar" },
      { nombre: "Monagas", abre: "mong" },
      { nombre: "Yaracuy", abre: "yar" },
      { nombre: "Zulia", abre: "zul" },


    ]
    
  }

}
