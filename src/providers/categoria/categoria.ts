import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CategoriaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriaProvider {

  public url: string;

  constructor(public http: HttpClient) {
     
      this.url = 'http://localhost:8080/api';
    
  }

  getCategorias(){
    return this.http.get(this.url + '/categorias');

  }
 
  getCategoriaID(id){
    return this.http.get(this.url+'/categorias/'+id);

  }

  guardarCategoria(categoria) {
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

    this.http.post(this.url + '/categorias', JSON.stringify(categoria), { headers: headers })
      .subscribe((data: any) => {
        console.log("Categoria registrada");
      },
        (error: any) => {
          console.dir(error);
        });
  }

  
}
