import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PublicacionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PublicacionProvider {
  public url:string;
  public userDetails: any;
  public perfil: any;

  constructor(public http: HttpClient) {
    this.url ='http://localhost:8080/api';
  }

  getAnuncios(){
    return this.http.get(this.url +'/publicaciones');
  }

  getAnuncioId(id){
    return this.http.get(this.url+'/publicaciones/'+id);
  }

  getAnuncioCategoria(id){
    return this.http.get(this.url+'/busqueda/categoria/'+id);
  }

  getAnuncioUsuario(){
    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    this.perfil = this.userDetails.usuario; 
    return this.http.get(this.url + '/publicaciones/usuario/' + this.perfil._id);
  }

  guardarPublicacion(publicacion){
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    this.perfil = this.userDetails.usuario; 
    this.http.post(this.url +'/publicar/'+this.perfil._id, JSON.stringify(publicacion), { headers: headers })
      .subscribe((data: any) => {
        console.log("Publicacion registrada");
      },
        (error: any) => {
          console.dir(error);
        });
  }

  modificarPublicacion(id,publicacion){
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

    this.http.put(this.url + '/publicaciones/' + id, publicacion, headers)
      .subscribe((publicacion:any)=>{
        console.log("Publicacion modificada")
    },
    (error: any) => {
      console.dir(error);
    });
  }

  borrarPublicacion(id){

    this.http.delete(this.url+"/publicaciones/"+id).subscribe((publicacion:any)=>{
      console.log("borrado exitosaamente")
    },
      (error: any) => {
        console.dir(error);
    });
  }

  
}
