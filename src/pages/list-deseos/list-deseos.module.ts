import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDeseosPage } from './list-deseos';

@NgModule({
  declarations: [
    ListDeseosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDeseosPage),
  ],
})
export class ListDeseosPageModule {}
