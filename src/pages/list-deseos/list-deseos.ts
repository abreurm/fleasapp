import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnunciosProvider } from '../../providers/anuncios/anuncios';
import { DetalleArticuloPage } from '../detalle-articulo/detalle-articulo';

/**
 * Generated class for the ListDeseosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-deseos',
  templateUrl: 'list-deseos.html',
})
export class ListDeseosPage {
  public listAnuncios: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public anuncios: AnunciosProvider) {
  }

  ionViewDidLoad() {
    this.listAnuncios=this.anuncios.listaFav;
  }

  goToDetalles(anuncio) {
    this.navCtrl.push(DetalleArticuloPage, { anuncio: anuncio, marcar: 'si' });
  }

}
