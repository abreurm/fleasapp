import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { PublicacionProvider } from '../../providers/publicacion/publicacion';

/**
 * Generated class for the MisPublicacionesDetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mis-publicaciones-detalles',
  templateUrl: 'mis-publicaciones-detalles.html',
})
export class MisPublicacionesDetallesPage {

  public anuncio: any;
  public anuncioArr: any;
  public marcar: string;
  public estado: string;
  public titulo: any;
  public precio: any;
  public descripcion: any;
  public fecha: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ctrlView: ViewController,
    public ctrlAlert: AlertController,
    public publicacion: PublicacionProvider) {
    this.marcar = this.navParams.get('marcar');


  }

  ionViewDidLoad() {
    this.anuncio = this.navParams.get('anuncio');
    Array.from(this.anuncio);
    this.titulo = this.anuncio.titulo;
    this.precio = this.anuncio.precio;
    this.descripcion = this.anuncio.descripcion;
    this.estado = this.anuncio.estado.nombre;
    this.fecha=this.anuncio.fecha;


    console.log(this.anuncio);
  }

  agregar() {
    this.marcar = "si";
    console.log(this.anuncio.titulo);
  }

  quitar() {
    this.marcar = "no";
    console.log(this.anuncio);
  }

  comprar() {
    let alerta = this.ctrlAlert.create({
      title: 'Ya casi lo tienes',
      subTitle: 'Le enviaremos un correo con los datos del vendedor para que concrete su compra',
      buttons: ['Aceptar']
    }
    );
    alerta.present();

  }
}