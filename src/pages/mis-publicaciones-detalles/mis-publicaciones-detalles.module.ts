import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisPublicacionesDetallesPage } from './mis-publicaciones-detalles';

@NgModule({
  declarations: [
    MisPublicacionesDetallesPage,
  ],
  imports: [
    IonicPageModule.forChild(MisPublicacionesDetallesPage),
  ],
})
export class MisPublicacionesDetallesPageModule {}
