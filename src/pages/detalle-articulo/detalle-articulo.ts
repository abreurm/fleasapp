import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import {PublicacionProvider} from '../../providers/publicacion/publicacion';

/**
 * Generated class for the DetalleArticuloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-articulo',
  templateUrl: 'detalle-articulo.html',
})
export class DetalleArticuloPage {
 public anuncio:any;
 public anuncioArr:any;
 public marcar:string;
 public estado:string;
 public titulo:any;
 public precio:any;
 public descripcion:any;
 public uso:any;
 public fecha:string;
 public direccion:String;
 public url: String;
 public nombre:String;
 public apellido:String;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    public ctrlView: ViewController,
    public ctrlAlert: AlertController,
    public publicacion: PublicacionProvider) {
    this.marcar=this.navParams.get('marcar');
   
    
  }

  ionViewDidLoad() {
    this.anuncio = this.navParams.get('anuncio');
    Array.from(this.anuncio);
    this.titulo=this.anuncio.titulo;
    this.precio=this.anuncio.precio;
    this.descripcion=this.anuncio.descripcion;
    this.estado=this.anuncio.estado.nombre;
    this.fecha= this.anuncio.fecha;
    this.direccion=this.anuncio.direccion;
    this.url=this.anuncio.url;
    this.nombre=this.anuncio.creador.nombre;
    this.apellido=this.anuncio.creador.apellido;
    console.log(this.anuncio);
  }

  agregar(){
    this.marcar = "si";
  }

  quitar(){
    this.marcar = "no";
  }

  comprar(){
    let alerta=this.ctrlAlert.create({
      title: 'Ya casi lo tienes',
      subTitle: 'Le enviaremos un correo con los datos del vendedor para que concrete su compra',
      buttons: ['Aceptar']
    }
    );

    alerta.present();
  }

  preguntar() {
    let alerta = this.ctrlAlert.create({
      title: 'Alerta',
      subTitle: 'Esta opción será implementada en futuras actualizaciones',
      buttons: ['Aceptar']
    }
    );

    alerta.present();
  }

}
