import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaProvider} from '../../providers/categoria/categoria';
import {EstadosProvider} from '../../providers/estados/estados';
import { ToastProvider } from '../../providers/toast/toast';
import {PublicacionProvider} from '../../providers/publicacion/publicacion';
import {AnunciosPage} from '../anuncios/anuncios';
/**
 * Generated class for the ProductosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
})
export class ProductosPage {
  public listCategoria: any;
  public listEstado:any;
  public formProducto: FormGroup;
  
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public categoria: CategoriaProvider,
    public estado: EstadosProvider,
    public form: FormBuilder,
    public toast: ToastProvider,
    public loading: LoadingController,
    public publicacion: PublicacionProvider
  ) {
      this.listCategoria=[];
      this.listEstado=[];   
      this.formProducto=this.form.group({
        titulo: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
        precio: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
        descripcion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(500)]],
        categoria: ["", [Validators.required]],
        estado: ["", [Validators.required]],
        direccion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(50)]]
       // usuario: ["",[Validators.required]]
      });
  }

  ionViewDidLoad() {
    this.categoria.getCategorias().subscribe(categorias=>{
      this.listCategoria = categorias;
    });
   this.estado.getEstados().subscribe(estados=>{
     this.listEstado=estados;
   });
  }

  enviarFormArticulos() {
    this.publicacion.guardarPublicacion(this.formProducto.value);
    let loader = this.loading.create({
      content: "Publicando...",
      duration: 3000
    });
    console.log(this.formProducto.value);
    loader.present();
    this.navCtrl.setRoot(AnunciosPage);
    this.toast.crearToast('Articulo publicado');

  }

}
