import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnunciosProvider} from '../../providers/anuncios/anuncios';
import { ProductosPage } from '../productos/productos';
import {DetalleArticuloPage} from '../detalle-articulo/detalle-articulo';
/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {
  public listPrincipal:any;
  public listAnuncios:any;
  public listMasB:any;
  public articulos = ProductosPage;
  public userDetails:any;
  public perfil:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public anuncios: AnunciosProvider) {
    this.listMasB=[];  
    this.listPrincipal=[];
  }

  ionViewDidLoad() {
    this.listMasB=this.anuncios.listaAnuncios;
    this.listAnuncios = this.anuncios.listaFav;
    this.listPrincipal=this.anuncios.listPrincipal;
    
    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    this.perfil = this.userDetails.usuario; 
    console.log("Hola:"+this.perfil.nombre);
   

  }

  goToDetalles(articulo) {
    this.navCtrl.push(DetalleArticuloPage, { anuncio: articulo, marcar: 'no' });
  }


}
