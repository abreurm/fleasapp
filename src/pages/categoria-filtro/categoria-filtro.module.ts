import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriaFiltroPage } from './categoria-filtro';

@NgModule({
  declarations: [
    CategoriaFiltroPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriaFiltroPage),
  ],
})
export class CategoriaFiltroPageModule {}
