import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PublicacionProvider} from '../../providers/publicacion/publicacion';
import {DetalleArticuloPage } from '../detalle-articulo/detalle-articulo';

/**
 * Generated class for the CategoriaFiltroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categoria-filtro',
  templateUrl: 'categoria-filtro.html',
})
export class CategoriaFiltroPage {
  public listAnuncios:any;
  public titulo:string;
  public id:string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public publicacion: PublicacionProvider) {
    this.listAnuncios=[];
    this.titulo = this.navParams.get('nombre');
      console.log(this.titulo)
  }

  ionViewDidLoad() {
    this.id=this.navParams.get('id');
    console.log(this.id)
    this.publicacion.getAnuncioCategoria(this.id).subscribe(anuncios=>{
      this.listAnuncios=anuncios;

      console.log(this.listAnuncios)
    })
  }

  goToDetalles(anuncio) {
    this.navCtrl.push(DetalleArticuloPage, { anuncio: anuncio, marcar: 'no' });
  }

}
