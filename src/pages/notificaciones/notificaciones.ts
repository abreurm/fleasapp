import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnunciosProvider } from '../../providers/anuncios/anuncios';

/**
 * Generated class for the NotificacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notificaciones',
  templateUrl: 'notificaciones.html',
})
export class NotificacionesPage {
  public listNotificaciones:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public notificaciones: AnunciosProvider) {
    this.listNotificaciones=[];
  }

  ionViewDidLoad() {
    this.listNotificaciones=this.notificaciones.listNotificaciones;
  }

}
