import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import {CategoriaProvider} from '../../providers/categoria/categoria';
import {CategoriaFiltroPage} from '../categoria-filtro/categoria-filtro';
import{PublicacionProvider} from '../../providers/publicacion/publicacion';
/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {
  public listCategorias:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public categoriaPr:  CategoriaProvider,
    public publicacion: PublicacionProvider) {
    this.listCategorias=[];
  }

  ionViewDidLoad() {
    this.categoriaPr.getCategorias().subscribe(categorias=>{
      this.listCategorias=categorias;
      console.log(this.listCategorias);
    })
  }

  goToCategoria(categoria){   
    this.navCtrl.setRoot(CategoriaFiltroPage, {nombre: categoria.nombre, id: categoria._id })        
  }

}
