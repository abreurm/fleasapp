import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaProvider } from '../../providers/categoria/categoria';
import { EstadosProvider } from '../../providers/estados/estados';
import { ToastProvider } from '../../providers/toast/toast';
import { PublicacionProvider } from '../../providers/publicacion/publicacion';
import { AnunciosPage } from '../anuncios/anuncios';

/**
 * Generated class for the EditProductoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-producto',
  templateUrl: 'edit-producto.html',
})
export class EditProductoPage {
  public formProducto: FormGroup;
  public anuncio:any;
  public id:string;
  public titulo:string;
  public precio:Number;
  public direccion:string;
  public descripcion:string;
  public listCategoria: any;
  public listEstado: any;
  public categoriaSelect: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public categoria: CategoriaProvider,
    public estado: EstadosProvider,
    public form: FormBuilder,
    public toast: ToastProvider,
    public loading: LoadingController,
    public publicacion: PublicacionProvider
  ) {
    this.formProducto = this.form.group({
      titulo: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      precio: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      descripcion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(500)]],
    /*  categoria: ["", [Validators.required]],
      estado: ["", [Validators.required]],*/
      direccion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(50)]]
    });
    this.anuncio=[];
  }

  ionViewDidLoad() {
    this.categoria.getCategorias().subscribe(categorias => {
      this.listCategoria = categorias;
    });
    this.estado.getEstados().subscribe(estados => {
      this.listEstado = estados;
    });
    this.anuncio = this.navParams.get('anuncio');
    Array.from(this.anuncio);
    this.id= this.anuncio._id;
    this.titulo = this.anuncio.titulo;
    this.precio = this.anuncio.precio;
    this.descripcion = this.anuncio.descripcion;
    this.direccion = this.anuncio.direccion;

    console.log(this.anuncio);

  
  }

  enviarFormArticulos() {
    console.log("este es el id:"+this.id);
    this.publicacion.modificarPublicacion(this.id,this.formProducto.value);
    let loader = this.loading.create({
      content: "Modificando...",
      duration: 3000
    });
    console.log(this.formProducto.value);
    loader.present();
    this.navCtrl.setRoot(AnunciosPage);
    this.toast.crearToast('Publicacion modificada');

  }

}
