import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PublicacionProvider } from '../../providers/publicacion/publicacion';
import { ProductosPage} from '../productos/productos';
import { DetalleArticuloPage } from '../detalle-articulo/detalle-articulo';
/**
 * Generated class for the AnunciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-anuncios',
  templateUrl: 'anuncios.html',
})
export class AnunciosPage {
  public listAnuncios: any;
  public anunciosIDs: any;
  public articulos = ProductosPage ;
  public titulo: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public anuncios: PublicacionProvider) {
    this.titulo = "Anuncios";
  
    this.anunciosIDs=[];
    this.listAnuncios=[];
      
    
  }

  ionViewDidLoad() {
    this.anuncios.getAnuncios().subscribe(anuncios => {
      this.listAnuncios = anuncios;
      console.log(anuncios);
    });

    
  }

  goToDetalles(anuncio){
    this.navCtrl.push(DetalleArticuloPage, { anuncio: anuncio, marcar:'no'});
  }


}
