import { Component } from '@angular/core';
import { IonicPage,NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { InicioPage } from '../../pages/inicio/inicio';
import { ToastProvider } from '../../providers/toast/toast';
import { EstadosProvider } from '../../providers/estados/estados';
import { UsuarioProvider } from '../../providers/usuario/usuario';



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public registro: string;
  public formRegistro: FormGroup;
  public formInicio: FormGroup;
  public tipoInput: string = 'password';
  public listEstados: any;
  public validarCorreo: any;
  public user:any;
  public perfil:any;
  public perfilUser:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public form: FormBuilder,
    public toast: ToastProvider,
    public loading: LoadingController,
    public usuario: UsuarioProvider,
    public estado: EstadosProvider,
    public alertCtrl: AlertController
  ) {
    this.estado.getEstados().subscribe(estado => {
      this.listEstados = estado;
      console.log(estado);
    });
    this.registro = 'si';
    this.formInicio = this.form.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.pattern(/^[a-z0-9_-]{6,18}$/)]]
    })

    this.formRegistro = this.form.group({
      nombre: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      apellido: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.pattern(/^[a-z0-9_-]{6,18}$/)]],
      estado: ["", [Validators.required]],
      direccion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(300)]],
      celular: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]]
    })

    this.user=[];
    this.perfil=[];
  }

  mostrar() {
    this.tipoInput = 'text';
  }

  ocultar() {
    this.tipoInput = 'password';
  }
  enviarFormInicio() {
    this.usuario.getCorreo(this.formInicio.value.email).subscribe(usuario=>{
      if(usuario.toString()!="no encontrado"){
        this.user=usuario;
    
       console.log(this.user.usuario.password);
        if(this.user.usuario.password==this.formInicio.value.password){
          console.log("usuario:"+this.user.usuario)
          localStorage.setItem('usuario', JSON.stringify(this.user));
          this.navCtrl.setRoot(InicioPage);
          this.toast.crearToast('Bienvenido '+this.user.usuario.nombre);
          this.perfilUser =JSON.parse(localStorage.getItem('usuario'));
        }else{
          let alerta = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Usuario o contraseña incorrecta',
          buttons: ['Aceptar']
          }
          );
          alerta.present()
        }      
      }else{
        let alerta = this.alertCtrl.create({
        title: 'Error',
          subTitle: 'Usuario o contraseña incorrecta',
        buttons: ['Aceptar']
        }
        );
        alerta.present()
      }
    });

  }

  enviarFormRegistro() {
    this.usuario.getCorreo(this.formRegistro.value.email).subscribe(usuario => {
      if (usuario.toString() == "no encontrado"){
        this.validarCorreo = usuario;
        this.usuario.guardarUsuario(this.formRegistro.value);
        let loader = this.loading.create({
          content: "Registrando...",
          duration: 3000
        })
        loader.present();
        console.log(this.formRegistro.value)
        this.registro = "si"
        this.toast.crearToast('Por favor inicie sesion');
      } else { // si esta registrado
        let alerta = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'El email ya se encuentra registrado',
          buttons: ['Aceptar']
        }
        );
        alerta.present();
      }
            
    })
  }
}
