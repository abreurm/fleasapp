import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';


/**
 * Generated class for the MiCuentaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mi-cuenta',
  templateUrl: 'mi-cuenta.html',
})
export class MiCuentaPage {
 public nombre:String;
 public apellido:String;
 public correo: String;
 public estado:String;
 public celular:String;
 public direccion:String;
 public userDetails:any;
 public perfil:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public usuario: UsuarioProvider
  ) {
    
  }
  ionViewDidLoad() {
    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    this.perfil = this.userDetails.usuario;    
  //  Array.from(this.userData);
    this.nombre = this.perfil.nombre;
    this.apellido = this.perfil.apellido;
    this.celular = this.perfil.celular;
    this.direccion = this.perfil.direccion;
    this.estado = this.perfil.estado.nombre;
    this.correo = this.perfil.email;
  }

}
