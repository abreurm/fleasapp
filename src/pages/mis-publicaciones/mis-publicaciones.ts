import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{PublicacionProvider} from'../../providers/publicacion/publicacion';
import {MisPublicacionesDetallesPage} from '../mis-publicaciones-detalles/mis-publicaciones-detalles';
import {EditProductoPage} from '../edit-producto/edit-producto'

/**
 * Generated class for the MisPublicacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mis-publicaciones',
  templateUrl: 'mis-publicaciones.html',
})
export class MisPublicacionesPage {
  public listAnuncios: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public anuncios:PublicacionProvider ) {
      this.listAnuncios=[];
  }


  ionViewDidLoad() {
    this.cargarLista();
  }

  cargarLista(){
    this.anuncios.getAnuncioUsuario().subscribe(anuncios => {
      this.listAnuncios = anuncios;
      console.log(this.listAnuncios);
    });    
  }

  borrar(anuncio) {
    this.anuncios.borrarPublicacion(anuncio);
    console.log("hello it's me"+anuncio);
    this.cargarLista();
  }

  actualizar(anuncio){
    console.log("estoy mandando esto: " + anuncio)
    this.navCtrl.push(EditProductoPage, {anuncio: anuncio});
  }

  ver(anuncio){
    console.log("estoy mandando esto: " + anuncio)
    this.navCtrl.push(MisPublicacionesDetallesPage, {anuncio: anuncio});
  }
}
