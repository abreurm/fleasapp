import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { InicioPage } from '../pages/inicio/inicio';
import { AnunciosPage } from '../pages/anuncios/anuncios';
import { CategoriasPage } from '../pages/categorias/categorias';
import { ListDeseosPage } from '../pages/list-deseos/list-deseos';
import { MensajesPage } from '../pages/mensajes/mensajes';
import { MiCuentaPage } from '../pages/mi-cuenta/mi-cuenta';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { ProductosPage } from  '../pages/productos/productos';
import  {DetalleArticuloPage } from '../pages/detalle-articulo/detalle-articulo';
import { CategoriaFiltroPage } from '../pages/categoria-filtro/categoria-filtro';
import { MisPublicacionesPage } from '../pages/mis-publicaciones/mis-publicaciones';
import {MisPublicacionesDetallesPage} from '../pages/mis-publicaciones-detalles/mis-publicaciones-detalles';
import {EditProductoPage} from '../pages/edit-producto/edit-producto';
import {LoginPage} from '../pages/login/login';

import { LoginComponent } from '../components/login/login';
import { MiHeaderComponent } from '../components/mi-header/mi-header';

import { AutoHideDirective } from '../directives/auto-hide/auto-hide';

import { AnunciosProvider } from '../providers/anuncios/anuncios';
import { CategoriasOfflineProvider } from '../providers/categorias-offline/categorias-offline';
import { ToastProvider } from '../providers/toast/toast';
import { CiudadProvider } from '../providers/ciudad/ciudad';
import { PublicacionProvider } from '../providers/publicacion/publicacion';
import { HttpClientModule } from '@angular/common/http';
import { CategoriaProvider } from '../providers/categoria/categoria';
import { EstadosProvider } from '../providers/estados/estados';
import { UsuarioProvider } from '../providers/usuario/usuario';


@NgModule({
  declarations: [
    MyApp,
    AutoHideDirective,
    MiHeaderComponent,
    InicioPage,
    AnunciosPage,
    LoginComponent,
    CategoriasPage,
    ListDeseosPage,
    MensajesPage,
    MiCuentaPage,
    NotificacionesPage,
    ProductosPage,
    DetalleArticuloPage,
    CategoriaFiltroPage,
    MisPublicacionesPage,
    MisPublicacionesDetallesPage,
    EditProductoPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InicioPage,
    AnunciosPage,
    LoginComponent,
    MiHeaderComponent,
    CategoriasPage,
    ListDeseosPage,
    MensajesPage,
    MiCuentaPage,
    NotificacionesPage,
    ProductosPage,
    DetalleArticuloPage,
    CategoriaFiltroPage,
    MisPublicacionesPage,
    MisPublicacionesDetallesPage,
    EditProductoPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AnunciosProvider,
    CategoriasOfflineProvider,
    ToastProvider,
    CiudadProvider,
    PublicacionProvider,
    CategoriaProvider,
    EstadosProvider,
    UsuarioProvider
  ]
})
export class AppModule {}
