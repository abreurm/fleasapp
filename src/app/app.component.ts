import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { InicioPage } from '../pages/inicio/inicio';
import { AnunciosPage } from '../pages/anuncios/anuncios';
import { CategoriasPage } from '../pages/categorias/categorias';
import { ListDeseosPage } from '../pages/list-deseos/list-deseos';
import { MensajesPage } from '../pages/mensajes/mensajes';
import { MiCuentaPage } from '../pages/mi-cuenta/mi-cuenta';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import{MisPublicacionesPage } from '../pages/mis-publicaciones/mis-publicaciones';
import{LoginPage} from '../pages/login/login';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('navegacion') nav: Nav;
  private rootPage: any;
  private nombre:String;
  private correo:String;
  private userDetails:any;
  private perfil:any;
  public listOpciones: Array<{titulo: string, icono: string, pagina: any}>;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.rootPage = LoginPage;
    this.listOpciones=[
      { titulo: 'Inicio', icono: 'basket', pagina: InicioPage},
      { titulo: 'Anuncios', icono: 'browsers', pagina: AnunciosPage},
      { titulo: 'Lista de deseos', icono: 'bookmark', pagina: ListDeseosPage },
      { titulo: 'Categorías', icono: 'folder-open', pagina: CategoriasPage },
      { titulo: 'Mi cuenta', icono: 'contact', pagina: MiCuentaPage },
      { titulo: 'Notificaciones', icono: 'notifications', pagina: NotificacionesPage },
      { titulo: 'Mis mensajes', icono: 'chatboxes', pagina: MensajesPage },
      { titulo: "Mis Publicaciones", icono: 'create', pagina: MisPublicacionesPage},
      { titulo: "Cerrar sesión", icono: 'md-log-out', pagina: LoginPage}
    ]

    this.userDetails = JSON.parse(localStorage.getItem("usuario"));
    if(this.userDetails!=null){
      this.perfil = this.userDetails.usuario;
      this.nombre = this.perfil.nombre;
      this.correo = this.perfil.email;
    }
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  goToPage(opcion){
    this.nav.setRoot(opcion);
  }
}

