import { NgModule } from '@angular/core';
import { MiHeaderComponent } from './mi-header/mi-header';
import { LoginComponent } from './login/login';
@NgModule({
	declarations: [MiHeaderComponent,
    LoginComponent],
	imports: [],
	exports: [MiHeaderComponent,
    LoginComponent]
})
export class ComponentsModule {}
