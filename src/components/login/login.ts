import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { InicioPage } from '../../pages/inicio/inicio';
import { ToastProvider } from '../../providers/toast/toast';
import {EstadosProvider} from   '../../providers/estados/estados';
import{UsuarioProvider}  from'../../providers/usuario/usuario'

/**
 * Generated class for the LoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginComponent {
  public registro: string;
  public formRegistro: FormGroup;
  public formInicio: FormGroup;
  public tipoInput: string = 'password';
  public listEstados:any;
  public validarCorreo:String;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public form: FormBuilder,
    public toast: ToastProvider,
    public loading: LoadingController,
    public usuario: UsuarioProvider,
    public estado:EstadosProvider,
    public alertCtrl: AlertController
    ){
    this.estado.getEstados().subscribe(estado => {
      this.listEstados = estado;
      console.log(estado);
    });
    this.registro = 'si';
    this.formInicio = this.form.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.pattern(/^[a-z0-9_-]{6,18}$/)]]
    })

    this.formRegistro = this.form.group({
      nombre: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      apellido: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.pattern(/^[a-z0-9_-]{6,18}$/)]],
      estado: ["", [Validators.required]],
      direccion: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(300)]],
      celular: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]]
    })
   
  }

  mostrar() {
    this.tipoInput = 'text';
  }

  ocultar() {
    this.tipoInput = 'password';
  }
  enviarFormInicio() {
    let loader = this.loading.create({
      content: "Por favor espere...",
      duration: 3000
    })
    loader.present();
    console.log(this.formInicio.value);

    this.navCtrl.setRoot(InicioPage);
    this.toast.crearToast('Bienvenido');

  }

  enviarFormRegistro(){
    this.usuario.getCorreo(this.formRegistro.value.email).subscribe(usuario=>{
      this.validarCorreo= usuario.toString();
      
     if (this.validarCorreo != "encontrado") { // si esta registrado
        this.usuario.guardarUsuario(this.formRegistro.value);
        let loader = this.loading.create({
          content: "Registrando...",
          duration: 3000
        })
        loader.present();
        console.log(this.formRegistro.value)
        this.navCtrl.setRoot(InicioPage);
        this.toast.crearToast('Bienvenido');
      }

      else { // si esta registrado
        let alerta = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'El email ya se encuentra registrado',
          buttons: ['Aceptar']
        }
        );
        alerta.present();
      }

    })   
   
  }

}
