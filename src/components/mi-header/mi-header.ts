import { Component, Input } from '@angular/core';

/**
 * Generated class for the MiHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mi-header',
  templateUrl: 'mi-header.html'
})
export class MiHeaderComponent {

  @Input()
  public titulo: string;

  constructor() {
  
  }

}
